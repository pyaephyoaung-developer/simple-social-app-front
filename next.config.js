module.exports = {
    reactStrictMode: true,
    serverRuntimeConfig: {
        secret: 'secret'
    },
    publicRuntimeConfig: {
        apiUrl: process.env.NODE_ENV === 'development'
            ? 'http://34.228.53.101/api'
            : 'http://34.228.53.101/api'
    }
}
