import { apiHandler, postsRepo, omit } from "helpers/api";
const jwt = require("jsonwebtoken");

export default apiHandler({
  get: getPosts,
  post: createPost,
});

function getPosts(req, res) {
  const response = postsRepo.getAll();
  response.sort(function (a, b) {
    return b.dateCreated.localeCompare(a.dateCreated);
  });
  return res.status(200).json(response);
}

function createPost(req, res) {
  const [, token] = req.headers.authorization.split(" ");
  const { userId } = jwt.decode(token);
  const post = { ...req.body, createdBy: userId };
  postsRepo.create(post);
  return res.status(200).json({});
}
