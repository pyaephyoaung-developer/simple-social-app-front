const bcrypt = require('bcryptjs');

import { apiHandler } from 'helpers/api';
import { postsRepo, omit } from 'helpers/api';

export default apiHandler({
    get: getById,
    put: update,
    delete: _delete
});

function getById(req, res) {
    const post = postsRepo.getById(req.query.id);

    if (!post) throw 'Post Not Found';

    return res.status(200).json(omit(post, 'hash'));
}

function update(req, res) {
    const post = postsRepo.getById(req.query.id);

    if (!post) throw 'Post Not Found';

    // split out password from post details 
    const params = req.body;

    postsRepo.update(req.query.id, params);
    return res.status(200).json({});
}

function _delete(req, res) {
    postsRepo.delete(req.query.id);
    return res.status(200).json({});
}
