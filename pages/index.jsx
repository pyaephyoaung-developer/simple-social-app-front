import { postService, userService } from "services";
import { Link } from "components";
import { useEffect, useState } from "react";
import Avatar from "react-avatar";
import moment from "moment";

import styles from "./index.module.css";

export default Home;

function Home() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const fetchPosts = function () {
      postService.getAll().then(async (allPosts) => {
        const data = [];
        for (const post of allPosts) {
          post.user = await userService.getById(post.createdBy);
          data.push(post);
        }
        setPosts(data);
      });
    };
    setInterval(fetchPosts, 10 * 1000); // refresh always 10 seconds
    fetchPosts();
  }, []);

  return (
    <div className="p-4">
      <div className="container">
        <h1>Hi {userService.userValue?.firstName}!</h1>
        <p>
          <Link href="/posts">Manage Posts</Link>
        </p>
        {posts.length > 0 &&
          posts.map((post) => {
            const ownerName = `${post.user?.firstName}`;
            return (
              <div key={post.id} className={styles.post}>
                <Avatar
                  round={true}
                  name={ownerName}
                  size="40"
                  className={styles.avatar}
                ></Avatar>
                {ownerName}
                <div className={styles.content}>{post.content}</div>
                <div>{moment(post.dateCreated).fromNow()}</div>
              </div>
            );
          })}
      </div>
    </div>
  );
}
