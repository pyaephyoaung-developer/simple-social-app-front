import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import { Link } from 'components';
import { postService, alertService } from 'services';

export { AddEdit };

function AddEdit(props) {
    const post = props?.post;
    const isAddMode = !post;
    const router = useRouter();
    
    // form validation rules 
    const validationSchema = Yup.object().shape({
        content: Yup.string()
            .required('Content is required'),
    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    // set default form values if in edit mode
    if (!isAddMode) {
        formOptions.defaultValues = props.post;
    }

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        return isAddMode
            ? createPost(data)
            : updatePost(post.id, data);
    }

    function createPost(data) {
        return postService.create(data)
            .then(() => {
                alertService.success('Post added', { keepAfterRouteChange: true });
                router.push('.');
            })
            .catch(alertService.error);
    }

    function updatePost(id, data) {
        return postService.update(id, data)
            .then(() => {
                alertService.success('Post updated', { keepAfterRouteChange: true });
                router.push('..');
            })
            .catch(alertService.error);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-row">
                <div className="form-group col">
                    <label>Content</label>
                    <textarea name="content" {...register('content')} className={`form-control ${errors.content ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.content?.message}</div>
                </div>
            </div>
            <div className="form-group">
                <button type="submit" disabled={formState.isSubmitting} className="btn btn-primary mr-2">
                    {formState.isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                    Save
                </button>               
                <Link href="/posts" className="btn btn-link">Cancel</Link>
            </div>
        </form>
    );
}